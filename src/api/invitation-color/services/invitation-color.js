'use strict';

/**
 * invitation-color service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::invitation-color.invitation-color');
