'use strict';

/**
 * invitation-color router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::invitation-color.invitation-color');
