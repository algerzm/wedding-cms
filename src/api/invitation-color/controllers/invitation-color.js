'use strict';

/**
 *  invitation-color controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::invitation-color.invitation-color');
