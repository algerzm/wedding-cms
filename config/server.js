module.exports = ({ env }) => ({
  host: env("HOST", "74.208.37.21"),
  port: env.int("PORT", 1337),
  url: env("PUBLIC_URL", "http://algerzm.me:1337"),
  app: {
    keys: env.array("APP_KEYS"),
  },
});
